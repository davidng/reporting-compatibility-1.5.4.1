/**
 * The contents of this file are subject to the OpenMRS Public License
 * Version 1.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://license.openmrs.org
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * Copyright (C) OpenMRS, LLC.  All Rights Reserved.
 */
package org.openmrs.module.reportingcompatibility;

/**
 * Constants required by this module
 */
public class ReportingCompatibilityConstants {
	
	//module id or name
	public static final String MODULE_ID = "reportingcompatibility";
	
	public static final String OPENMRS_REPORT_DATA = "__openmrs_report_data";
	public static final String OPENMRS_REPORT_ARGUMENT = "__openmrs_report_argument";
	
	/**
	 * GP name for the customization property
	 */
	public static final String BATCH_SIZE_GP = MODULE_ID + ".data_export_batch_size";
	
	/**
	 * Default value if the user hasn't filled in the previous gp
	 */
	public static final Integer BATCH_SIZE_GP_DEFAULT = 7500;
	
	// added by Citigodev1		
	/**
	 * The key for the global property to set list of cohortIds that should be visible
	 */
	public static String COHORT_ID_SHOW_LIST_GLOBAL_PROPERTY_NAME = MODULE_ID + ".cohortIdShowList";
	
	/**
	 * The key for the global property to set list of savedSearchIds that should be visible
	 */
	public static String SAVED_SEARCH_ID_SHOW_LIST_GLOBAL_PROPERTY_NAME = MODULE_ID + ".savedSearchIdShowList";
	
	/**
	 * The key for the global property to set which role of the user is used to get Cohort definitions
	 * 
	 */
	public static String GLOBAL_PROPERTY_COHORT_USER_ROLE = MODULE_ID + ".cohort.";
	
	/**
	 * The key for the global property to set which role of the user is used to get Cohort definitions
	 * 
	 */
	public static String GLOBAL_PROPERTY_SAVED_SEARCH_USER_ROLE = MODULE_ID + ".savedsearch.";

}
